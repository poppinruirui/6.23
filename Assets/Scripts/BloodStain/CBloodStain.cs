﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class CBloodStain : MonoBehaviour {
    public GameObject m_goMainContainer;

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();
    static Color colorTemp = new Color();

    CBloodStainManager.eBloodStainType m_eType = CBloodStainManager.eBloodStainType.dead_spray;
    CBloodStainManager.eFadeMode m_eFadeMode = CBloodStainManager.eFadeMode.common;

    public SpriteRenderer m_srMain;

    float m_fLastTime = 10f;
    float m_fFadeTime = 2f;
    float m_fFadeTimeElapse = 0f;
    bool m_bLasting = false;
    bool m_bFading = false;

    float m_fDynamicScaleTime = 0.5f;

    // Use this for initialization
    void Start () {
		
	}

    public void Reset()
    {
        m_srMain.material.SetFloat("_FillAmount", 0);
        m_srMain.material.SetFloat("_FadingPercent", 0 );
    }

    // Update is called once per frame
    void Update () {
        FaddeLoop();
        LastLoop();
        DynamicScaleLoop();
        FillLoop();
    }

    public CBloodStainManager.eBloodStainType GetStainType()
    {
        return m_eType;
    }

    public void SetActive(bool bActive)
    {
        this.transform.gameObject.SetActive(bActive);

        if (bActive)
        {

        }
        else
        {
          
        }
    }


    public void SetStainType(CBloodStainManager.eBloodStainType eType )
    {
        m_eType = eType;
    }

    public void SetPos( Vector3 pos )
    {
        pos.z = 900f;
        this.transform.position = pos;
    }

    public void SetDir( Vector3 dir )
    {
        float fAngle = CyberTreeMath.Dir2Angle( dir.x, dir.y );
        this.transform.localRotation = Quaternion.identity;
        this.transform.Rotate(0.0f, 0.0f, fAngle);
    }

    float m_fScale = 1f;
    public void setScale( float fScaleX, float fScaleY)
    {
        m_fScale = fScaleX;
        vecTempScale.x = fScaleX;
        vecTempScale.y = fScaleY;
        vecTempScale.z = 1f;
        this.transform.localScale = vecTempScale;
    }

    bool m_bDynamicScale = false;
    public void BeginDynamicScale(float fDynamicScaleTime = 0.5f)
    {
        vecTempScale.x = 0;
        vecTempScale.y = 0;
        vecTempScale.z = 1f;
        this.transform.localScale = vecTempScale;
        m_bDynamicScale = true;
        m_fDynamicScaleTime = fDynamicScaleTime;
    }

    void DynamicScaleLoop()
    {
        if (!m_bDynamicScale)
        {
            return;
        }

        float fCurScale = this.transform.localScale.x;
        fCurScale += Time.deltaTime * m_fScale / m_fDynamicScaleTime;
        vecTempScale.x = fCurScale;
        vecTempScale.y = fCurScale;
        vecTempScale.z = 1f;
        this.transform.localScale = vecTempScale;
        if (fCurScale >= m_fScale)
        {
            EndDynamicScale();
        }
    }

    void EndDynamicScale()
    {
        m_bDynamicScale = false;
    }

    public void SetColor( Color color )
    {
        m_srMain.color = color;
    }

    public void SetLastTime( float fLastTime )
    {
        m_fLastTime = fLastTime;
    }

    public void SetFadeTime(float fFadeTime )
    {
        m_fFadeTime = fFadeTime;
    }

    public void BeginLast()
    {
        m_bLasting = true;
    }

    void LastLoop()
    {
        if ( !m_bLasting)
        {
            return;
        }
        m_fLastTime -= Time.deltaTime;
        if (m_fLastTime <= 0)
        {
            EndLast();
            BeginFade();
        }
    }

    void EndLast()
    {
        m_bLasting = false;
    }

    public void BeginFade()
    {
        m_bFading = true;
        m_fFadeTimeElapse = 0;
    }

    void FaddeLoop()
    {
        if (!m_bFading)
        {
            return;
        }
      
        if ( m_eFadeMode == CBloodStainManager.eFadeMode.common )
        {
            FadeLoop_Common();
        }
        else if (m_eFadeMode == CBloodStainManager.eFadeMode.fill)
        {
            FadeLoop_Fill();
        }

    }

    void FadeLoop_Common()
    {
        m_fFadeTimeElapse += Time.deltaTime;

        colorTemp = m_srMain.color;
        colorTemp.a = (m_fFadeTime - m_fFadeTimeElapse) / m_fFadeTime;
        m_srMain.color = colorTemp;

        if (m_fFadeTimeElapse >= m_fFadeTime)
        {
            EndFade();
        }
    }

    void FadeLoop_Fill()
    {
        m_fFadeTimeElapse += Time.deltaTime;
        float fPercent = m_fFadeTimeElapse / m_fFadeTime;
        if (fPercent >= 1f)
        {
            EndFade();
        }
        m_srMain.material.SetFloat("_FadingPercent", fPercent);
    }

    void EndFade()
    {
        m_bFading = false;
        CBloodStainManager.s_Instance.DeleteBloodStain_SprayMode( this );
    }

    bool m_bFilling = false;
    float m_fFillTotalTime = 0f;
    float m_fFillAmount = 0f;
    public void BeginFill( float fTotalTime )
    {
        m_fFillAmount = 0f;
        m_fFillTotalTime = fTotalTime;
        m_bFilling = true;
        m_srMain.material.SetFloat("_FillAmount", 0f );
    }

    void FillLoop()
    {
        if (!m_bFilling)
        {
            return;
        }

        m_fFillAmount += Time.deltaTime / m_fFillTotalTime;
        m_srMain.material.SetFloat("_FillAmount", m_fFillAmount);
        if (m_fFillAmount >= 1f)
        {
            EndFill();
        }
    }

    void EndFill()
    {
        m_bFilling = false;
    }

    public void SetFadeMode( CBloodStainManager.eFadeMode fade_mode )
    {
        m_eFadeMode = fade_mode;
    }
    // =============================== new blood stain =======================
    uint m_uKey = 0;
    uint m_uParentKey = 0;
    CBlockDivide.eBlockPosType m_ePosType = CBlockDivide.eBlockPosType.none;
    CBlockDivide.eBlockPosType m_eParentPosType = CBlockDivide.eBlockPosType.none;
    int m_nHierarchy = 0;
    int m_nColorId = 0;



    public SpriteRenderer m_sprMain;
    public SortingGroup _sortingGroup;


    public void SetHierarchy(int nHierarchy)
    {
        m_nHierarchy = nHierarchy;
    }

    public int GetHierarchy()
    {
        return m_nHierarchy;
    }


    public Vector3 GetPos()
    {
        return this.transform.position;
    }

    public void SetScale(Vector3 scale)
    {
        this.transform.localScale = scale;
    }

    public Vector3 GetScale()
    {
        return this.transform.localScale;
    }

  
    public void SetKey(uint uKey)
    {
        m_uKey = uKey;
    }

    public void SetParentKey(uint uParentKey)
    {
        m_uParentKey = uParentKey;
    }

    public uint GetParentKey()
    {
        return m_uParentKey;
    }

    public uint GetKey()
    {
        return m_uKey;
    }

    public void SetPosType(CBlockDivide.eBlockPosType eType)
    {
        m_ePosType = eType;
    }

    public CBlockDivide.eBlockPosType GetPosType()
    {
        return m_ePosType;
    }

    public void SetParentPosType(CBlockDivide.eBlockPosType eType)
    {
        m_eParentPosType = eType;
    }

    public CBlockDivide.eBlockPosType GetParentPosType()
    {
        return m_eParentPosType;
    }

    public void TryLightParent()
    {
       

        // 计算父色块的ID
        int nHierarchy = GetHierarchy();
        uint uBase = CBlockDivide.s_Instance.GetHierarchyReverseKey(nHierarchy);
        uint uParentKey = GetKey() / uBase * uBase;



      

        for (int i = 1; i <= 4; i++)
        {
            if (i == (int)GetPosType())
            {
                continue;
            }

            uint uNeighborKey = uParentKey + (uint)i * CBlockDivide.s_Instance.GetHierarchyKeybase(nHierarchy);

            CBloodStain stain = CBloodStainManager.s_Instance.GetBloodStain_SprayMode(uNeighborKey);
            if (stain == null || stain.GetColorId() != GetColorId())
            { // 邻居色块只要有一个为null或者不同色，则无法点亮父色块，直接退出
                return;
            }

        } // end for

        // ?? 待定（这里绝对不会存在自己的父色块。但有可能存在别人点亮的父色块）
        CBloodStain parent_stain = CBloodStainManager.s_Instance.GetBloodStain_SprayMode(uParentKey);
        if (parent_stain != null)
        {
            Debug.LogError("parent_stain != null"); // 父色块此时是不可能已经存在的，要么一直不存在；要么本来是其它颜色的父色块，被打碎了
            return;
        }

        parent_stain = CBloodStainManager.s_Instance.NewBloodStain_SprayMode(uParentKey);
        parent_stain.SetKey(uParentKey);
        parent_stain.SetScale(2f * GetScale());
        parent_stain.SetPosType(GetParentPosType());
        parent_stain.SetHierarchy(nHierarchy - 1);
        parent_stain.SetColorId(GetColorId());

        // 该色块已点亮，其对应的区块可以销毁了
        CBlock parent_block = CBlockDivide.s_Instance.GetBlockByKey(uParentKey);
        if (parent_block != null)
        {
            CBlockDivide.s_Instance.DeleteBlock(parent_block);  
        }

        float fHalfScale = GetScale().x * 0.5f;
        switch (GetPosType())
        {
            case CBlockDivide.eBlockPosType.left_up:
                {
                    vecTempPos.x = GetPos().x + fHalfScale;
                    vecTempPos.y = GetPos().y - fHalfScale;
                }
                break;
            case CBlockDivide.eBlockPosType.right_up:
                {
                    vecTempPos.x = GetPos().x - fHalfScale;
                    vecTempPos.y = GetPos().y - fHalfScale;
                }
                break;
            case CBlockDivide.eBlockPosType.left_down:
                {
                    vecTempPos.x = GetPos().x + fHalfScale;
                    vecTempPos.y = GetPos().y + fHalfScale;
                }
                break;
            case CBlockDivide.eBlockPosType.right_down:
                {
                    vecTempPos.x = GetPos().x - fHalfScale;
                    vecTempPos.y = GetPos().y + fHalfScale;
                }
                break;
        }; // end switch
        vecTempPos.z = 0;
        parent_stain.SetPos(vecTempPos);

        // 销毁该父对象下的四个色块
        for (int i = 1; i <= 4; i++)
        {
            uint uNeighborKey = uParentKey + (uint)i * CBlockDivide.s_Instance.GetHierarchyKeybase(nHierarchy);
            CBloodStain stain = CBloodStainManager.s_Instance.GetBloodStain_SprayMode(uNeighborKey);
            if (stain == null)
            {
                continue;
            }

            CBloodStainManager.s_Instance.DeleteBloodStain(stain);
        } // end for

        if (!IsHighest())
        {
            parent_stain.TryLightParent();
        }
    }

    public bool IsHighest()
    {
        return GetHierarchy() == CBlockDivide.HIGHEST_HIERARCHY_NO;
    }

    public void SetColorId(int nColorId)
    {
        m_nColorId = nColorId;

        UpdateColor();

        _sortingGroup.sortingOrder = m_nColorId + 1;
    }

    public void UpdateColor()
    {
        colorTemp = ResourceManager.GetPlayerColorById(m_nColorId);
        colorTemp.a = 0.8f;
        m_sprMain.color = colorTemp;//CBlockDivide.s_Instance.GetColorByPlayerId(m_nColorId);
    }

    public int GetColorId()
    {
        return m_nColorId;
    }

    public void Fade(float fAmount)
    {
        _sortingGroup.sortingOrder = 0;
        colorTemp = m_sprMain.color;
        colorTemp.a -= fAmount;
        if (colorTemp.a < 0)
        {
            colorTemp.a = 0;
        }
        m_sprMain.color = colorTemp;
    }

    public bool IsFadingCompleted()
    {
        return m_sprMain.color.a <= 0;
    }
}


