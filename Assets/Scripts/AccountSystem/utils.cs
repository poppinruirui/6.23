using System;
using System.IO;
using UnityEngine;

public class Utils
{
    public static string platformBundlePath()
    {
        switch (Application.platform) {
            default:
            case RuntimePlatform.WindowsEditor:
            case RuntimePlatform.WindowsPlayer:
                return "bundles/Windows";                
            case RuntimePlatform.OSXEditor:
            case RuntimePlatform.OSXPlayer:
                return "bundles/OSX";
            case RuntimePlatform.IPhonePlayer:
                return "bundles/iOS";
            case RuntimePlatform.Android:
                return "bundles/Android";
        }
    }

    public static string platformBundleListName()
    {
        switch (Application.platform) {
            default:
            case RuntimePlatform.WindowsEditor:                
            case RuntimePlatform.WindowsPlayer:
                return "Windows";                
            case RuntimePlatform.OSXEditor:                
            case RuntimePlatform.OSXPlayer:
                return "OSX";
            case RuntimePlatform.IPhonePlayer:
                return "iOS";
            case RuntimePlatform.Android:
                return "Android";
        }
    }

    public static string spritePathToBundlePath(string spritePath)
    {
        var bundlePath = spritePath.Replace("images", platformBundlePath());
        bundlePath = bundlePath.Replace(".png", "");
        return bundlePath;
    }    

    public static string platformBundleListPath()
    {
        return platformBundlePath() + "/" + platformBundleListName();
    }
    
    public static string bundlePathToName(string bundlePath)
    {
        return bundlePath.Replace(platformBundlePath() + "/", "");
    }
    
    public static string bundlePathToRelativeName(string bundlePath)
    {
        return bundlePath.Substring(bundlePath.LastIndexOf("/", bundlePath.Length-1, bundlePath.Length, StringComparison.Ordinal) + 1);
    }    

    public static string bundleFilePath(string bundlePath)
    {
        return bundlePath + ".bdle";
    }

    public static string bundleHashPath(string bundlePath)
    {
        return bundlePath + ".hash";
    }

    public static string prefixInteger(int num, int length)
    {
        return num.ToString().PadLeft(length,'0');
    }

    public static bool getFirstRunning()
    {
        if (PlayerPrefs.GetInt("FIRST_RUNNING", 1) == 1) {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static void setFirstRunningDone()
    {
        PlayerPrefs.SetInt("FIRST_RUNNING", 0);
    }

    public static string urlToFilePath(string url)
    {
        return url.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
    }
};
