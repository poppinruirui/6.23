﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCosmosGunSight : MonoBehaviour {

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    public GameObject m_goHead;
    public GameObject m_goJoint;
    public GameObject m_goStick;

    float m_fAngle = 0f;
    float m_fChildSize = 0f;
    float m_fDistanceForStick = 0f;
    float m_fTotalDistance = 0f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    bool m_bPicked = false;
    public void SetPicked( bool bPicked )
    {
        m_bPicked = bPicked;
    }

    public bool IsPicked()
    {
        return m_bPicked;
    }

    public void GetParams( ref float fAngle, ref float fChildSize, ref float fDistanceForStick, ref float fTotalDistance )
    {
        fAngle = m_fAngle;
        fChildSize = m_fChildSize;
        fDistanceForStick = m_fDistanceForStick;
        fTotalDistance = m_fTotalDistance;
    }

    // MainPlayer and OtherClient
    public void UpdateStatus( Vector3 pos, float fAngle, float fChildSize, float fDistanceForStick, float fTotalDistance )
    {
        this.transform.localRotation = Quaternion.identity;
        this.transform.Rotate(0.0f, 0.0f, fAngle);

        this.transform.position = pos;


        vecTempScale.x = fChildSize;
        vecTempScale.y = fDistanceForStick;
        vecTempScale.z = 0f;
        m_goStick.transform.localScale = vecTempScale;

        vecTempScale.x = fChildSize;
        vecTempScale.y = fChildSize;
        vecTempScale.z = 0f;
        m_goJoint.transform.localScale = vecTempScale;
        vecTempPos.x = 0;
        vecTempPos.y = fDistanceForStick;
        vecTempPos.z = 0;
        m_goJoint.transform.localPosition = vecTempPos;

        vecTempScale.x = fChildSize;
        vecTempScale.y = fChildSize;
        vecTempScale.z = 0f;
        m_goHead.transform.localScale = vecTempScale;
        vecTempPos.x = 0;
        vecTempPos.y = fTotalDistance;
        vecTempPos.z = 0;
        m_goHead.transform.localPosition = vecTempPos;
    }

    // MainPlayer
    public void UpdateStatus( float fTotalDistance,  Vector3 pos, Vector2 dir, float fMotherRadius, float fChildSize, float fDistanceMultiple)
    {
        m_fTotalDistance = fTotalDistance;
        m_fChildSize = fChildSize;
        m_fAngle = CyberTreeMath.Dir2Angle(dir.x, dir.y) - 90f;
        m_fDistanceForStick = fTotalDistance - fChildSize;

        UpdateStatus(pos, m_fAngle, m_fChildSize, m_fDistanceForStick, fTotalDistance);

    }

    
    
}
