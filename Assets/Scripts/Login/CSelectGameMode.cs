﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// FFFF12

public class CSelectGameMode : MonoBehaviour {

    public static CSelectGameMode s_Instance;

    public GameObject _panelSelectGameMode;
    public GameObject _panelSelectSkill;

    public eGameMode m_eGameMode = eGameMode.daluangdou;

    public GameObject[] _goContainerPages;
    int m_nCurSelectedPageIndex = 0;

    public CButtonClick[] _aryPageBtns;

    public Text _txtPlayerName;

    public enum eGameMode
    {
        daluangdou,
        wangzhongwang,
    };

    public Button _btnSelectGameMode_DaLuanDou;
    public Button _btnSelectGameMode_WangZhongWang;

    public CGameModeSelectCounter _counterDaLuanDou;
    public CGameModeSelectCounter _counterDaWangZHongWang;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
        vecTempScale.x = 1.17f;
        vecTempScale.y = 1.17f;
        vecTempScale.z = 1f;
        _aryPageBtns[0].transform.localScale = vecTempScale;
        _aryPageBtns[0].SetButtonStatusImage(CButtonClick.eButtonStatus.down);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public eGameMode GetGameMode()
    {
        return m_eGameMode;
    }

    public void OnButtonClick_SelectGameMode_DaLuanDou()
    {
        m_eGameMode = eGameMode.daluangdou;
        _counterDaLuanDou.SetSelected( true );
        _counterDaWangZHongWang.SetSelected(false);
        //EnterSelectSkillPage();
    }

    public void OnButtonClick_SelectGameMode_WangZhongWang()
    {
        m_eGameMode = eGameMode.wangzhongwang;
        _counterDaLuanDou.SetSelected(false);
        _counterDaWangZHongWang.SetSelected(true);

       // EnterSelectSkillPage();
    }

    public void EnterSelectSkillPage()
    {
        _panelSelectGameMode.SetActive( false );
        _panelSelectSkill.SetActive(true);
		CAudioManager.s_Instance.PlayAudio ( CAudioManager.eAudioId.e_audio_flip_page );
    }

    public void EnterDiyPage()
    {
        CMsgBox.s_Instance.Show( "该功能尚未开放" );
    }

    public void EnterPage_0()
    {
        EnterPage(0);
    }

    public void EnterPage_1()
    {
        EnterPage(1);
    }

    public void EnterPage_2()
    {
        EnterPage(2);
    }

    public void EnterPage_3()
    {
        EnterPage(3);
    }

    static Vector3 vecTempScale = new Vector3();
    public void EnterPage( int nPageIndex )
    {
        for ( int i = 0; i < _goContainerPages.Length; i++ )
        {
            _goContainerPages[i].SetActive( false );
            _aryPageBtns[i].SetButtonStatusImage( CButtonClick.eButtonStatus.common );
            vecTempScale.x = 1f;
            vecTempScale.y = 1f;
            vecTempScale.z = 1f;
            _aryPageBtns[i].transform.localScale = vecTempScale;
        }
        _goContainerPages[nPageIndex].SetActive(true);
        _aryPageBtns[nPageIndex].SetButtonStatusImage(CButtonClick.eButtonStatus.down);
        m_nCurSelectedPageIndex = nPageIndex;
        vecTempScale.x = 1.17f;
        vecTempScale.y = 1.17f;
        vecTempScale.z = 1f;
        _aryPageBtns[nPageIndex].transform.localScale = vecTempScale;

    }

    public void OnClickButton_Friend()
    {
        CMsgBox.s_Instance.Show( "该功能尚未开放" );
    }

}
