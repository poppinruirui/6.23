﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class CPlayerManager : MonoBehaviour {

    public GameObject m_prePlayerIns;

    public class sPlayerInfoCommon
    {
        // 当前总体积不用同步，是根据队伍中所有球球的体积，实时计算出来的

        public short nLevel;        // player等级
        public short nExp;          // 经验值
        public short nMoney;        // 金币

        public float fBaseVolumeByLevel; // (随等级变化的)基础体积
        public float fBaseSpeed;  // 基础速度（使用道具改变的）
        public float fBaseShellTime; // 基础壳时间（使用道具改变的）

        public short nItem_BaseSpeed_level; // “基础速度”的道具当前等级
        public short nItem_BaseShellTime_level; // “基础壳时间”的道具当前等级

        public short nEatThornNum;  // 吃刺数

        public short nSelectedSkillId; // 选择的R技能是哪个
        public short nSkillWLevel; // w技能当前等级
        public short nSkillELevel; // E技能当前等级
        public short nSkillRLevel; // R技能当前等级

        public short nSkillPoint;  // 当前拥有的技能点

        public byte[] bytes = new byte[64];

        public byte[] GenerateDataBlob()
        {
            StringManager.BeginPushData(bytes);
            StringManager.PushData_Short(nLevel);
            StringManager.PushData_Short(nMoney);
            StringManager.PushData_Short(nExp);
            StringManager.PushData_Float(fBaseVolumeByLevel);
            StringManager.PushData_Float(fBaseSpeed);
            StringManager.PushData_Float(fBaseShellTime);
            StringManager.PushData_Short(nItem_BaseSpeed_level);
            StringManager.PushData_Short(nItem_BaseShellTime_level);
            StringManager.PushData_Short(nSelectedSkillId);
            StringManager.PushData_Short(nSkillWLevel);
            StringManager.PushData_Short(nSkillELevel);
            StringManager.PushData_Short(nSkillRLevel);
            StringManager.PushData_Short(nSkillPoint);
            StringManager.PushData_Short(nEatThornNum);
            return bytes;
        }

    };

    public enum ePlayerCommonInfoType
    {
        level,
        exp,
        money,
        base_basevolume_by_level,
        base_basespeed,
        base_baseshelltime,
        item_basespeed_level,
        item_baseshelltime_level,
        eat_thorn_num,
        skill_r_id,
        skill_w_level,
        skill_e_level,
        skill_r_level,
        skill_point,
    };



    Dictionary<string, sPlayerInfoCommon> m_dicPlayerInfo_Common = new Dictionary<string, sPlayerInfoCommon>();

    public void SetPlayerData_Common(string szAccount, ePlayerCommonInfoType type, float val)
    {
        sPlayerInfoCommon info = null;
        if (!m_dicPlayerInfo_Common.TryGetValue(szAccount, out info))
        {
            info = new sPlayerInfoCommon();
        }
        switch (type)
        {
            case ePlayerCommonInfoType.level:
                {
                    info.nLevel = (short)val;
                }
                break;
            case ePlayerCommonInfoType.exp:
                {
                    info.nExp = (short)val;
                }
                break;
            case ePlayerCommonInfoType.money:
                {
                    info.nMoney = (short)val;
                }
                break;
            case ePlayerCommonInfoType.base_basespeed:
                {
                    info.fBaseSpeed = val;
                }
                break;
            case ePlayerCommonInfoType.base_baseshelltime:
                {
                    info.fBaseShellTime = val;
                }
                break;
            case ePlayerCommonInfoType.base_basevolume_by_level:
                {
                    info.fBaseVolumeByLevel = val;
                }
                break;
            case ePlayerCommonInfoType.item_basespeed_level:
                {
                    info.nItem_BaseSpeed_level = (short)val;
                }
                break;
            case ePlayerCommonInfoType.item_baseshelltime_level:
                {
                    info.nItem_BaseShellTime_level = (short)val;
                }
                break;
            case ePlayerCommonInfoType.skill_r_id:
                {
                    info.nSelectedSkillId = (short)val;
                }
                break;
            case ePlayerCommonInfoType.skill_w_level:
                {
                    info.nSkillWLevel = (short)val;
                }
                break;
            case ePlayerCommonInfoType.skill_e_level:
                {
                    info.nSkillELevel = (short)val;
                }
                break;
            case ePlayerCommonInfoType.skill_r_level:
                {
                    info.nSkillRLevel = (short)val;
                }
                break;
            case ePlayerCommonInfoType.skill_point:
                {
                    info.nSkillPoint = (short)val;
                }
                break;
            case ePlayerCommonInfoType.eat_thorn_num:
                {
                    info.nEatThornNum = (short)val;
                }
                break;
        }

        m_dicPlayerInfo_Common[szAccount] = info;
    }

    public void GetPlayerData_Common(string szAccount, ref sPlayerInfoCommon info)
    {
        info = null;
        if (!m_dicPlayerInfo_Common.TryGetValue(szAccount, out info))
        {
            info = null;
        }
    }


    public static CPlayerManager s_Instance = null;

    Dictionary<int, Player> m_dicPlayers = new Dictionary<int, Player>();

    // [to youhua] 超高频度的TryGetValue会不会造成性能问题？稍后测试以及优化一下
    Dictionary<string, byte[]> m_dicPlayerInfo_RealTime = new Dictionary<string, byte[]>();
    public void SetPlayerData_RealTime(string szAccount, byte[] bytes)
    {
        m_dicPlayerInfo_RealTime[szAccount] = bytes;
    }

    public byte[] GetPlayerData_RealTime(string szAccount)
    {
        byte[] bytes = null;
        if (!m_dicPlayerInfo_RealTime.TryGetValue(szAccount, out bytes)) {
            return null;
        }
        return bytes;
    }

    void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public void AddPlayer(Player player)
    {
        m_dicPlayers[player.GetOwnerId()] = player;
    }

    public Player GetPlayer(int nOwnerId)
    {
        Player player = null;
        if (!m_dicPlayers.TryGetValue(nOwnerId, out player))
        {
            player = null;
        }
        return player;
    }

    public void RecordTotalEatArea(int nPlayerId, float val)
    {

    }

    public void RecordWhoKillWhom(int nEaterId, int nDeadMeatId)
    {
        Player playerEater = GetPlayer(nEaterId);
        playerEater.IncKillCount();
        Player playerDeadMeat = GetPlayer(nDeadMeatId);
        playerDeadMeat.IncBeKilledCount();
    }

    public string GenerateOnePlayerInfo(Player player)
    {
        string szPlayerContent = "";
        szPlayerContent += "玩家名字      ：" + player.GetPlayerName() + "\n";
        szPlayerContent += "击杀次数      ：" + player.GetKillCount() + "\n";
        szPlayerContent += "被击杀次数   ：" + player.GetBeKilledCount() + "\n";
        szPlayerContent += "累计吃球体积：" + player.GetTotalEatArea() + "\n";
        szPlayerContent += "_____________________________________________________________________\n\n";

        return szPlayerContent;
    }

    string m_szShengFuPlayerName = "";
    public void SetShengFuPanDingPlayer(string szShengFuPlayerName)
    {
        m_szShengFuPlayerName = szShengFuPlayerName;
    }

    public string GetJieSuanInfo()
    {
        string szContent = "";

        szContent += "吃掉决胜球的玩家：" + m_szShengFuPlayerName + "\n\n";

        szContent += GenerateOnePlayerInfo(Main.s_Instance.m_MainPlayer);

        foreach (KeyValuePair<int, Player> pair in m_dicPlayers)
        {
            if (pair.Key == Main.s_Instance.m_MainPlayer.GetOwnerId())
            {
                continue;
            }
            szContent += GenerateOnePlayerInfo(pair.Value);
        }

        return szContent;
    }

    public void SetSomeThingVisible(float fThreshold, float fLowerThreshold, bool bVisible)
    {
        foreach (KeyValuePair<int, Player> pair in m_dicPlayers)
        {
            List<Ball> lst = null;
            pair.Value.GetBallList(ref lst);
            for (int i = 0; i < lst.Count; i++)
            {
                Ball ball = lst[i];

                if (ball.IsDead() || !ball.GetActive())
                {
                    continue;
                }

                if (!bVisible)
                {
                    if (ball.transform.localScale.x < fThreshold)
                    {
                        ball.SetPlayerNameVisible(false);
                        ball.SetStaticShellVisible(false);
                        ball.SetDynamicShellVisible(false);
                    }
                }
                else
                {
                    if (ball.transform.localScale.x < fThreshold && ball.transform.localScale.x >= fLowerThreshold)
                    {
                        ball.SetPlayerNameVisible(true);
                        ball.SetStaticShellVisible(true);
                        ball.SetDynamicShellVisible(true);
                    }
                }
            } // end for 
        } // end foreach
    }

    Dictionary<string, CPlayerIns> m_dicPlayerIns = new Dictionary<string, CPlayerIns>();
    public GameObject m_goContainerPlayerIns;
    public void TryInitBallsForThisPlayer( byte[] bytesBallsData, string szAccount, int nSkinId, int nColorId, string szPlayerName)
    {
        CPlayerIns playerIns = GetPlayerInsByAccount(szAccount);
        if (playerIns == null)
        {
            playerIns = CreatePlayerIns(szAccount);
        }
        Player player = GetPlayerByAccount(szAccount);
        playerIns.SetPlayer(player);
        if (player != null) // 如果遇到孤儿球，这里是会为null的
        {
            player.SetPlayerIns(playerIns);
        }
        playerIns.BeginInitBalls(bytesBallsData, szAccount, nSkinId, nColorId, szPlayerName);
    }

    public CPlayerIns CreatePlayerIns( string szAccount )
    {
        CPlayerIns playerIns = GameObject.Instantiate(m_prePlayerIns).GetComponent<CPlayerIns>();
        playerIns.transform.SetParent(m_goContainerPlayerIns.transform);
        playerIns.SetAccount(szAccount);
        playerIns.gameObject.name = szAccount;
        SetPlayerInsByAccount(szAccount, playerIns);
        return playerIns;
    }

    public CPlayerIns GetPlayerInsByAccount( string szAccount )
    {
        CPlayerIns playerIns = null;
        if ( !m_dicPlayerIns.TryGetValue(szAccount, out playerIns))
        {
            playerIns = null;
        }
        return playerIns;
    }

    public void SetPlayerInsByAccount(string szAccount, CPlayerIns playerIns)
    {
        m_dicPlayerIns[szAccount] = playerIns;
    }

    Dictionary<string, Player> m_dicPlayerByAccount = new Dictionary<string, Player>();
    public Player GetPlayerByAccount( string szAccount)
    {
        Player player = null;
        if ( !m_dicPlayerByAccount.TryGetValue(szAccount, out player))
        {
            player = null;
        }
        return player;
    }

    public void SetPlayerByAccount(string szAccount, Player player)
    {
        m_dicPlayerByAccount[szAccount] = player;
    }

    public byte[] GetPlayerData_BallData( string szAccount )
    {
        byte[] bytes = null;

        CPlayerIns playerIns = GetPlayerInsByAccount(szAccount);
        if (playerIns == null)
        {
            return null;
        }

        int nBlobSize = 0;
        bytes = playerIns.GenerateLiveBallData( ref nBlobSize);

        return bytes;
    }

    // 注意：StringManager目前是“静态+单线程”模式的，如果同时解析多个Blob，内部指针会错乱！！！！！！！

    byte[] _byteOrphanBallDatat = new byte[102400]; // [to youhua]这里要优化一下，做成分帧机制，因为数据量可能很大
    
    public byte[] GenerateOrphanBallsData()
    {
        m_lstTempOrphanBallData.Clear();
        foreach ( KeyValuePair<string, CPlayerIns> pair in m_dicPlayerIns )
        {
            if ( pair.Value._Player != null ) // _Player为null就表示这个玩家掉线了，它旗下的球球就是“孤儿球”
            {
                continue;
            }

            sOrphanInfo info;
            info.szAccount = pair.Value.GetAccount(); ;
            info.szPlayerName = pair.Value.GetPlayerName();
            info.nSkinId = pair.Value.GetSkinId();
            info.nColorId = pair.Value.GetColorId();
            info.nBlobSize = 0;
            info.bytesBallsData = pair.Value.GenerateLiveBallData(ref info.nBlobSize);
            if (info.bytesBallsData == null) // 孤儿球留在场景上被别人吃完了
            {
                continue;
            }

            m_lstTempOrphanBallData.Add(info);
        }

        StringManager.BeginPushData(_byteOrphanBallDatat);
        StringManager.PushData_Int(0);
        int nNum = m_lstTempOrphanBallData.Count;
        for ( int i = 0; i < m_lstTempOrphanBallData.Count; i++ )
        {
            sOrphanInfo info = m_lstTempOrphanBallData[i];

            // 账号
            int nLenOfAccount = StringManager.GetStringLength(info.szAccount);
            StringManager.PushData_Short((short)nLenOfAccount);
            StringManager.PushData_String(info.szAccount, nLenOfAccount);

            // 昵称
            int nLenOfPlayerName = StringManager.GetStringLength(info.szPlayerName);
            StringManager.PushData_Short((short)nLenOfPlayerName);
            StringManager.PushData_String(info.szPlayerName, nLenOfPlayerName);

            // 色号
            StringManager.PushData_Short((short)info.nColorId);

            // 皮肤号
            StringManager.PushData_Short((short)info.nSkinId);

            StringManager.PushData_Short((short)info.nBlobSize);
            StringManager.PushData_Blob(info.bytesBallsData, info.nBlobSize);
        }

        int nTotalBlobSize = StringManager.GetBlobSize();
        StringManager.SetCurPointerPos(0);
        StringManager.PushData_Int(nNum);
        if (nNum == 0)
        {
            return null;
        }
        else
        {
            return _byteOrphanBallDatat.Take(nTotalBlobSize).ToArray();
        }
    }

    public struct sOrphanInfo
    {
        public string szAccount;
        public string szPlayerName;
        public int nColorId;
        public int nSkinId;
        public int nBlobSize;
        public byte[] bytesBallsData;
    };
   

    List<sOrphanInfo> m_lstTempOrphanBallData = new List<sOrphanInfo>(); 
    public void AnalyzeOrphanBallData( byte[] bytes )
    {
        if (bytes == null)
        {
            return;
        }

        
        m_lstTempOrphanBallData.Clear();
        StringManager.BeginPopData(bytes);
        int nNum = StringManager.PopData_Int();
        for ( int i = 0; i < nNum; i++ )
        {
            sOrphanInfo info;
            int nLenOfAccount = StringManager.PopData_Short();
            info.szAccount = StringManager.PopData_String(nLenOfAccount);
            int nLenOfPlayerName = StringManager.PopData_Short();
            info.szPlayerName = StringManager.PopData_String(nLenOfPlayerName);
            info.nColorId = StringManager.PopData_Short();
            info.nSkinId = StringManager.PopData_Short();
            info.nBlobSize = StringManager.PopData_Short();
            info.bytesBallsData = StringManager.PopData_Blob(info.nBlobSize);
            m_lstTempOrphanBallData.Add(info);
        }

        // 这儿必须用一个List缓存一下，再解析每个Blob的数据。因为StringManager目前是“静态+单线程”模式的，如果同时解析多个Blob，内部指针会错乱

        for ( int i = 0; i < m_lstTempOrphanBallData.Count; i++ )
        {
            sOrphanInfo info = m_lstTempOrphanBallData[i];
            TryInitBallsForThisPlayer(info.bytesBallsData, info.szAccount, info.nSkinId, info.nColorId, info.szPlayerName);
        }
        
    }
}

