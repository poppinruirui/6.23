﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ListItem : MonoBehaviour {

	public Text _txtContent;
	public Button _btnSelect;

	string m_szContent = "";

	int m_nIndex = 0;

	public ComoList _MyList;

	// Use this for initialization
	void Start () {
		_btnSelect.onClick.AddListener ( OnClick );
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnClick()
	{
		//Debug.Log ( "选中了：" + m_szContent );
		_MyList.Select( this );
	}

	public void SetContent( string szContent )
	{
		m_szContent = szContent;
		_txtContent.text = m_szContent;
	}

	public void SetIndex( int nIndex )
	{
		m_nIndex = nIndex;
	}

	public int GetIndex()
	{
		return m_nIndex;
	}

	public string GetContent()
	{
		return m_szContent;
	}

	public void SetSize( float fWidth, float fHeight )
	{
		RectTransform rectTransform = _btnSelect.gameObject.GetComponent<RectTransform>();
		rectTransform.sizeDelta = new Vector2(fWidth, fHeight);
		rectTransform = _txtContent.gameObject.GetComponent<RectTransform>();
		rectTransform.sizeDelta = new Vector2(fWidth, fHeight);
	}
}
