﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DistrictAndRoomManager : MonoBehaviour {

    public static DistrictAndRoomManager s_Instance = null;

    //// !---- UI
    public Text _txtCurAccount;
    public Text _txtHostAccount;
    public Text _txtHostOrGuest;
    public Text _txtCurSelectedDistrict;
    public Text _txtCurSelectedRoom;
    public Dropdown _dropdownDistrict;
    public Dropdown _dropdownRoom;

    void Awake()
    {
        s_Instance = this;
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetHostAccount( string szHostAccount )
    {

    }

    public void SetCurAccount( string szCurAccount )
    {

    }

    public void BackToHomePage()
    {

    }

    public void CreateRoomInSelectedDistrict()
    {

    }

    public void EnterSelectedRoom()
    {

    }

    public void CloseSelectedRoom()
    {

    }

    public void EditSelectedDistrict()
    {

    }

    public void CreateNewDistrict()
    {

    }

    public void DeleteSelectedDistrict()
    {

    }
}
